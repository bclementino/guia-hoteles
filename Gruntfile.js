module.exports = function (grunt) {
  require("time-grunt")(grunt);
  require("jit-grunt")(grunt, {
    useminPrepare: "grunt-usemin",
  });
  grunt.initConfig({
    sass: {
      dist: {
        files: [
          {
            expand: true,
            cwd: "css",
            src: ["*.scss"],
            dest: "css",
            ext: ".css",
          },
        ],
      },
    },
    watch: {
      files: ["css/*.scss"],
      tasks: ["css"],
    },
    browserSync: {
      dev: {
        bsFiles: {
          // Browser Files
          src: ["css/*.css", "*.html", "js/*.js"],
        },
        options: {
          watchTask: true,
          server: {
            baseDir: "./", //Directorio base para nuestro servidor
          },
        },
      },
    },
    imagemin: {
      dynamic: {
        files: [
          {
            expand: true,
            cwd: "./",
            src: "images/*.{png.gif,jpg,jpeg}",
            dist: "dist/",
          },
        ],
      },
    },

    copy: {
      html: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: "./", //current working directory
            src: ["*.html"],
            dest: "dist",
          },
        ],
      },
      fonts: {
        files:[{
          expand: true,
          dot: true,
          cwd: "/node_modules/@fortawesome/fontawesome-free/webfonts",
          src: ["*.*"],
          dest: "dist"
        }]
      }
    },
    clean: {
      build: {
        src: ["./dist"], //clean the distribution folder
      },
    },
    cssmin: {
      dist: {},
    },
    uglify: {
      dist: {},
    },
    filerev: {
      options: {
        encoding: "utf8",
        algorithm: "md5",
        length: 20,
      },
      release: {
        //filerev:release hashes (md5) all assets (images, js and css)
        //in dist directory
        files: [
          {
            src: ["dist/css/*.css", "dist/js/*.js"],
          },
        ],
      },
    },
    concat: {
      options: {
        separator: ";",
      },
      dist: {},
    },
    useminPrepare: {
      foo: {
        dest: "dist",
        src: ["index.html", "contacto.html", "about.html", "precios.html"],
      },
      options: {
        flow: {
          steps: {
            css: ["cssmin"],
            js: ["uglify"],
          },
          post: {
            css: [
              {
                name: "cssmin",
                createConfig: function (context, block) {
                  var generated = context.options.generated;
                  generated.options = {
                    keepSpecialComments: 0,
                    rebase: false,
                  };
                },
              },
            ],
          },
        },
      },
    },
    usemin: {
      html: [
        "dist/index.html",
        "dist/contacto.html",
        "dist/precios.html",
        "dist/about.html",
      ],
      options: {
        assetsDir: ["dist", "dist/css", "dist/js"],
      },
    },
  });
  
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-browser-sync");
  grunt.loadNpmTasks("grunt-contrib-imagemin");
  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks("grunt-contrib-cssmin");
  grunt.loadNpmTasks("grunt-usemin");
  grunt.loadNpmTasks("grunt-filerev");

  grunt.registerTask("css", ["sass"]);
  grunt.registerTask("img:compress", ["imagemin"]);
  /* grunt.registerTask("default", ["browserSync", "watch"]); */
  /*grunt.registerTask("build", [
    "clean", //Borramos el contenido de dist
    "copy", //Copiamos los archivos html a dist
    "imagemin", //Optimizamos imagenes y las copiamos a dist
    "useminPrepare", //Preparamos la configuracion de usemin
    "concat",
    "cssmin",
    "uglify",
    "filerev", //Agregamos cadena aleatoria
    "usemin", //Reemplazamos las referencias por los archivos generados por filerev
  ]); */
};
